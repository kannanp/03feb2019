package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class DuplicateLeadPage extends Annotations{
	
	public  DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(how=How.XPATH, using="//input[@class='smallSubmit']") WebElement eleDuplicateLead;
	
	public ViewLeadPage CreateDupicateLead() {
		click(eleDuplicateLead);
		
		System.out.println("lead duplicated");
		return new ViewLeadPage();
	}

}
