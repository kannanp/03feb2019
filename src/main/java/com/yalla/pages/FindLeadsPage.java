package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations{
	
	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
		
	}

	
	
	
	
	
	@FindBy(how=How.XPATH, using="//input[@name='id']") WebElement eleFindLeadByID;
	public FindLeadsPage EnterLeadID(String data) {
		
		clearAndType(eleFindLeadByID, data);
		return this;
	}
	@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]") WebElement eleFisrtName;
	public FindLeadsPage firstname(String data) {
		clearAndType(eleFisrtName, data);
		return this;
	}
	@FindBy(how=How.XPATH, using = "//button[text()='Find Leads']") WebElement eleclickFindLead;
	public FindLeadsPage FindleadSearch() {
		click(eleclickFindLead);
		return this;
	}
	@FindBy(how=How.XPATH, using="//a[text()='idhoparu']") WebElement eleLeadname;
	public ViewLeadPage eleLeadname() {
		click(eleLeadname);
		return new ViewLeadPage();
		
	}
	
}

