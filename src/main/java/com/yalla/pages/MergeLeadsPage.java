package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeadsPage extends Annotations{
	
	public MergeLeadsPage() {
		
		PageFactory.initElements(driver, this);
	}

	@FindBy (how=How.XPATH, using="//img[@src='/images/fieldlookup.gif']") WebElement eleLead1icon;
	
	public void clickLookUpIcon1() {
		click(eleLead1icon);
		switchToWindow(1);
	//	@FindBy(How.NAME, using="firstName") WebElement findFirstName;
	}


}
