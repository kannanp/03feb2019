package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLeadPageCucumber extends Annotations{

	public  CreateLeadPageCucumber() {
		PageFactory.initElements(driver, this);
		
	}
	
	
	
	@FindBy(how=How.ID, using="username")  WebElement eleUserName;
	@Given("Enter the username (.*)")
	public CreateLeadPageCucumber enterUserName(String data) {
		clearAndType(eleUserName, data);  
		return this; 
	}
	
	@FindBy(how=How.ID, using="password")  WebElement elePassWord;
	@Given("Enter the password (.*)")
	public CreateLeadPageCucumber enterPassWord(String data) {
		clearAndType(elePassWord, data); 
		return this; 
	}
	
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogin;
	@When("click the login button")
	public HomePage clickLogin() {
		  click(eleLogin);  
          return new HomePage();
	}
	
	@Then("verify login success")
	public void verifyLoginSuccess() {
	   System.out.println("login Success");
	    
	}
	
	@FindBy(how = How.LINK_TEXT, using= "CRM/SFA")  WebElement eleCrmsfa;
	@When("click crmsfa")
	public MyHomePage clickCRMSFA() {
		click(eleCrmsfa);
		return new MyHomePage();
	}
	
	@FindBy(how= How.LINK_TEXT, using= "Leads") WebElement eleLeads;
	@When("click LeadsTab")
	public MyLeadsPage clickLeads() {
		click(eleLeads);
		return new MyLeadsPage();
	}
	@FindBy(how=How.LINK_TEXT, using = "Create Lead") WebElement eleCreateLead;
	@When("click CreateLeadsTab")
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	@When("load CompanyName as(.*)")
	public CreateLeadPageCucumber enterCompanyName(String data) {
		clearAndType(eleCompanyName, data);
		return this;
		
	}
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	@When("load firstName as (.*)")
	public CreateLeadPageCucumber enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
		
	}
	@FindBy(how=How.ID, using ="createLeadForm_lastName") WebElement eleLastName;
	@When("load LastName as (.*)")
	public CreateLeadPageCucumber enterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
		
	}
	@FindBy(how=How.XPATH, using ="//input[@class='smallSubmit']") WebElement eleCreatLeadButton;
	@When("click CreateLead")
	public ViewLeadPage clickCreateLeadButton() {
		click(eleCreatLeadButton);
		return new ViewLeadPage();
		
	}
	
	@Then("verify createLeadSuccess")
	public void verifyCreateLeadSuccess() {
	   
	    
	}
	
}
