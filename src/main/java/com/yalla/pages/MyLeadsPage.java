package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations{
	
	public  MyLeadsPage() {
		PageFactory.initElements(driver, this);
		
	}
	@FindBy(how=How.LINK_TEXT, using = "Create Lead") WebElement eleCreateLead;
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	@FindBy(how=How.LINK_TEXT, using ="Find Leads") WebElement eleFindLeads;
	public FindLeadsPage clickFindLeads() {
		click(eleFindLeads);
		return new FindLeadsPage();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Merge Leads") WebElement eleMergeLead;
	public MergeLeadsPage clickMergeLeads() {
		click(eleMergeLead);
		return new MergeLeadsPage();
	}
}
