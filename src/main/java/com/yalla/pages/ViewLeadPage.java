package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations{

	public  ViewLeadPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	
	@FindBy(how=How.XPATH, using="//a[text()='Edit']") WebElement eleEditLead;
	public OpenTapsPage EditLead() {
		click(eleEditLead);
		return new OpenTapsPage();
		
	}
	@FindBy(how=How.LINK_TEXT, using="Duplicate Lead") WebElement eleDuplicateLead;
	public DuplicateLeadPage Duplicatelead() {
		click(eleDuplicateLead);
		return new DuplicateLeadPage();
	}
	
	@FindBy(how=How.LINK_TEXT, using="Delete") WebElement eleDelete;
	public MyLeadsPage DeleteLead() {
		click(eleDelete);
		return new MyLeadsPage();
		
	}
	
}
