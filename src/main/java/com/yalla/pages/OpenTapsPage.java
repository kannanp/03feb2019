package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class OpenTapsPage extends Annotations{
	
	public OpenTapsPage() {
	PageFactory.initElements(driver, this);	
	}

		
    @FindBy(how=How.ID, using ="updateLeadForm_firstName") WebElement eleUpdateFirstName;
	//@FindBy(how=How.XPATH, using="(//input[@name='firstName'])[3]") WebElement eleUpdateFirstName;
	public OpenTapsPage updateFirstNameLead(String data){
		clearAndType(eleUpdateFirstName, data);
		return this;
	}
	@FindBy(how=How.NAME, using ="submitButton") WebElement eleUpdate;
	public ViewLeadPage updateLead() {
		click(eleUpdate);
		return new ViewLeadPage();
		
		
	}
}
