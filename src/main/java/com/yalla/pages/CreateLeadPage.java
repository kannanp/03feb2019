package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations{

	public  CreateLeadPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement eleCompanyName;
	
	public CreateLeadPage enterCompanyName(String data) {
		clearAndType(eleCompanyName, data);
		return this;
		
	}
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement eleFirstName;
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
		
	}
	@FindBy(how=How.ID, using ="createLeadForm_lastName") WebElement eleLastName;
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
		
	}
	@FindBy(how=How.XPATH, using ="//input[@class='smallSubmit']") WebElement eleCreatLeadButton;
	public ViewLeadPage clickCreateLeadButton() {
		click(eleCreatLeadButton);
		return new ViewLeadPage();
		
	}
	
}
