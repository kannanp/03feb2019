package com.yalla.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.yalla.testng.api.base.Annotations;

public class LeaftapsPage extends Annotations{

	public LeaftapsPage() {
		
	}
	
	@FindBy(how = How.LINK_TEXT, using= "CRM/SFA")  WebElement eleCrmsfa;
		public MyHomePage clickCRMSFA() {
			click(eleCrmsfa);
			return new MyHomePage();
			
		}
		
	}
	

