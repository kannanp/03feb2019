package com.yalla.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.yalla.testng.api.base.Annotations;

public class AcmeTest {
	@Test
	public void Vendors() {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		
		driver.findElementById("email").sendKeys("kannanp.alr@gmail.com");
		driver.findElementById("password").sendKeys("welcome123");
		driver.findElementById("buttonLogin").click();
		
		Actions obj = new Actions(driver);
		WebElement eleVendor = driver.findElementByXPath("//button[text()=' Vendors']");
		obj.moveToElement(eleVendor).build().perform();
		
		driver.findElementByLinkText("Search for Vendor").click();
	
		driver.findElementById("vendorTaxID").sendKeys("RO892123");
		driver.findElementById("buttonSearch").click();
		String vendorName = driver.findElementByXPath("(//tr)[2]/td").getText();
		
		System.out.println(vendorName);
		
	}

}
