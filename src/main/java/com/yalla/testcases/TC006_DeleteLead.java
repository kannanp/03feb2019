package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC006_DeleteLead extends Annotations{
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_DeleteLead";
		testcaseDec = "Deleting a Lead";
		author = "Kannan";
		category = "smoke";
		excelFileName = "TC006";
	}

	@Test(dataProvider="fetchData")
	public void deleteLead(String uName, String pass, String leadDelete) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pass)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads() 
		.clickFindLeads()
		.firstname(leadDelete)
		.FindleadSearch()
		.eleLeadname()
		.DeleteLead();
		
	}
}
