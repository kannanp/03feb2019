package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_DuplicateLead extends Annotations{

	@BeforeTest
	public void setData() {
		testcaseName = "TC002_DuplicateLead";
		testcaseDec = "Duplicating a Lead";
		author = "Kannan";
		category = "smoke";
		excelFileName = "TC004";
}
	
	@Test(dataProvider="fetchData") 

	public void EditLead(String UserName, String pass, String first) {
		new LoginPage()
		.enterUserName(UserName)
		.enterPassWord(pass)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads() 
		.clickFindLeads()
		.firstname(first)
		.FindleadSearch()
		.eleLeadname()
		.Duplicatelead()
		.CreateDupicateLead();
			
			
		
			
			
		}
}
