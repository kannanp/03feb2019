package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.pages.MyLeadsPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Craeting a Lead";
		author = "Kannan";
		category = "smoke";
		excelFileName = "TC002";
	}
	@Test(dataProvider="fetchData") 
	public void CreateLead(String uname, String pass, String Cname, String FName, String Lname) {
		new LoginPage()
			.enterUserName(uname)
			.enterPassWord(pass)
			.clickLogin()
			.clickCRMSFA()
			.clickLeads()
			.clickCreateLead()
			.enterCompanyName(Cname)
			.enterFirstName(FName)
			.enterLastName(Lname)
			.clickCreateLeadButton();
		}
		
	}


