package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{

	@BeforeTest
	public void setData() {
		testcaseName="Edit Lead";
		testcaseDec = "Editing lead details";
		author = "Kanann";
		category = "smoke";
		excelFileName = "TC003";
	}
	
	@Test(dataProvider="fetchData")
	public void EditLead(String UserName, String pass, String fName, String Firstname){
		new LoginPage()
		.enterUserName(UserName)
		.enterPassWord(pass)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads() 
		.clickFindLeads()
		.firstname(fName)
		.FindleadSearch()
		.eleLeadname()
		.EditLead()
		.updateFirstNameLead(Firstname)
		.updateLead();
		}
}
