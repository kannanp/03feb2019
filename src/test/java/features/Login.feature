Feature: Login for leaftap application
Scenario: Positive login flow
Given open the browser
And maximize the browser
And load the url
And Enter the username
And Enter the password
When click the login button
Then verify login success
When click crmsfa
When click LeadsTab
When click CreateLeadsTab
And load CompanyName
And load firstName
And load LastName
When click CreateLead
Then verify createLeadSuccess