package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		
		features = "src\\test\\java\\features\\LoginTask.feature",
		glue = {"com.yalla.pages","steps"},
		monochrome=true
		/*dryRun=true,
		snippets=SnippetType.CAMELCASE	*/
		
		)

public class RunTest {
	

}
